var map = L.map('main_map').setView([51.505, -0.09], 13);

L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
}).addTo(map);

L.marker([51.5, -0.09]).addTo(map)
    .bindPopup('A pretty CSS3 popup.<br> Easily customizable.')
    .openPopup();


$.ajax({
    dataType: 'json',
    url: 'API/bicicletas',
    success: (result)=>{
        console.log(result);
        result.bicicletas.forEach(bici => {
            L.marker(bici.ubicacion,{title: bici.id}).addTo(map)
        });
    
    }
});